package Utilities;

/**
 * Just used to figure out properties of any class.
 *
 * @author CKeil
 *
 */
public class ShowProperties {

	public static void main(final String[] args) {

		System.getProperties().list(System.out);
	}
}
